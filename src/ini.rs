//! Freedesktop flavoured ini loader
//!
//! Based on information described by:
//! https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-1.2.html

use std::collections::HashMap;
use std::io::{BufRead, BufReader, Read, Result as IOResult};

#[derive(Debug, Default)]
pub struct Ini {
    contents: HashMap<String, HashMap<String, String>>,
}

impl Ini {
    pub fn load(stream: &mut Read) -> IOResult<Ini> {
        let buf_reader = BufReader::new(stream);

        let mut result = Ini::default();
        let mut current_group = "".to_string();
        for it in buf_reader.lines() {
            let line_raw = it?;
            let line = line_raw.trim();

            if line.is_empty() || line.starts_with('#') {
                // This line is an empty line or comment line
                continue;
            } else if line.starts_with('[') && line.ends_with(']') {
                // This line is a group header
                let name = line[1..line.len() - 1].trim();
                current_group = name.to_string();
            } else if let Some(index) = line.find('=') {
                // This line is an entry
                let (key_raw, value_raw) = line.split_at(index);
                let key = key_raw.trim();
                let value = value_raw[1..].trim();
                let group_contents = result.contents.entry(current_group.clone()).or_default();
                group_contents.insert(key.to_string(), value.to_string());
            } else {
                // This line is an unknown line
            }
        }

        Ok(result)
    }

    pub fn group(&self, name: &str) -> Option<&HashMap<String, String>> {
        self.contents.get(name)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn well_formed_ini() {
        let data = "
        [Icon Theme]
        # This is an icon theme file
        Name=Test
        Name[ja]=テスト
        Comment=Test Icon Theme
        Comment[ja]=テスト アイコン テーマ

        Inherits=Base
        # Inherits=Removed
        ";
        let mut stream = Cursor::new(data);
        let ini = Ini::load(&mut stream).unwrap();
        let group = ini.group("Icon Theme");
        assert!(group.is_some());

        let group = group.unwrap();
        assert_eq!(group.len(), 5);
        assert_eq!(group["Name"], "Test");
        assert_eq!(group["Name[ja]"], "テスト");
        assert_eq!(group["Comment"], "Test Icon Theme");
        assert_eq!(group["Comment[ja]"], "テスト アイコン テーマ");
        assert_eq!(group["Inherits"], "Base");
    }

    #[test]
    fn extra_spaces() {
        let data = "
         [  Icon Theme  ]            \n
        # This is an icon theme file
          Name        = Test
          Name[ja]    = テスト    \n
          Comment     = Test Icon Theme
          Comment[ja] = テスト アイコン テーマ

          Inherits=Base
          # Inherits=Removed
        ";
        let mut stream = Cursor::new(data);
        let ini = Ini::load(&mut stream).unwrap();
        let group = ini.group("Icon Theme");
        assert!(group.is_some());

        let group = group.unwrap();
        assert_eq!(group.len(), 5);
        assert_eq!(group["Name"], "Test");
        assert_eq!(group["Name[ja]"], "テスト");
        assert_eq!(group["Comment"], "Test Icon Theme");
        assert_eq!(group["Comment[ja]"], "テスト アイコン テーマ");
        assert_eq!(group["Inherits"], "Base");
    }
}
