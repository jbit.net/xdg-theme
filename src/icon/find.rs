use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::{i32, u32};

trait Theme {
    fn name(&self) -> &str;
    fn parents(&self) -> Vec<&Theme>;
    fn subdirs(&self) -> Vec<&ThemeSubdir>;
}

enum ThemeSizeType {
    Fixed,
    Scalable,
    Threshold,
    Other(String),
}

trait ThemeSubdir {
    fn path(&self) -> &Path;
    fn size(&self) -> u32;
    fn min_size(&self) -> u32;
    fn max_size(&self) -> u32;
    fn threshold(&self) -> u32;
    fn scale(&self) -> u32;
    fn size_type(&self) -> ThemeSizeType;

    fn matches_size(&self, size: u32, scale: u32) -> bool {
        if self.scale() != scale {
            return false;
        }
        match self.size_type() {
            ThemeSizeType::Fixed => self.size() == size,
            ThemeSizeType::Scalable => (size >= self.min_size()) && (size <= self.max_size()),
            ThemeSizeType::Threshold => (size >= self.size() - self.threshold()) && (size <= self.size() + self.threshold()),
            _ => false,
        }
    }

    fn size_distance(&self, size: u32, scale: u32) -> u32 {
        let scaled_size = size * scale;


        match self.size_type() {
            ThemeSizeType::Fixed => {
                i32::abs((self.size()*self.scale()) as i32 - scaled_size as i32) as u32
            }
            ThemeSizeType::Scalable => {
                let min_size = self.min_size()*self.scale();
                let max_size = self.max_size()*self.scale();
                if scaled_size < min_size{
                    min_size  - scaled_size
                } else if scaled_size > max_size {
                    scaled_size - max_size
                } else {
                    0
                }
            }
            ThemeSizeType::Threshold => {
                let min_size = (self.size() - self.threshold()) * self.scale();
                let max_size = (self.size() + self.threshold()) * self.scale();
                if scaled_size < min_size {
                    min_size - scaled_size
                } else if scaled_size > max_size {
                    scaled_size - max_size
                } else {
                    return 0
                }
            }
            _ => u32::MAX,
        }
    }
}


struct ThemeLookup {
    themes: Vec<Arc<Theme>>,
    search_path: Vec<PathBuf>,
    extensions: Vec<PathBuf>,
    check_exists: fn(path: &Path) -> bool,
}




impl ThemeLookup {
    pub fn find(&self, name: &str, size: u32, scale: u32) -> Option<PathBuf> {
        for theme in self.themes.iter() {
            self.find_helper(name, size, scale, theme.as_ref());
        }

        return self.lookup_fallback(name);
    }

    fn find_helper(&self, name: &str, size: u32, scale: u32, theme: &Theme) -> Option<PathBuf> {
        if let Some(found) = self.lookup(name, size, scale, theme) {
            return Some(found);
        }
        for parent in theme.parents() {
            if let Some(found) = self.find_helper(name, size, scale, parent) {
                return Some(found);
            }
        }
        return None;
    }

    fn lookup(&self, name: &str, size: u32, scale: u32, theme: &Theme) -> Option<PathBuf> {
        let mut minimal_distance = u32::MAX;
        let mut closest_filename: Option<PathBuf> = None;
        for subdir in theme.subdirs() {
            for basedir in &self.search_path {
                for extension in &self.extensions {
                    let filename = basedir.join(theme.name()).join(subdir.path()).join(Path::new(name).with_extension(extension));
                    if (self.check_exists)(&filename) {
                        if subdir.matches_size(size, scale) {
                            return Some(filename);
                        }
                        let distance = subdir.size_distance(size, scale);
                        if distance < minimal_distance {
                            closest_filename = Some(filename);
                            minimal_distance = distance;
                        }
                    }
                }
            }
        }

        closest_filename
    }

    fn lookup_fallback(&self, name: &str) -> Option<PathBuf> {
        for basedir in &self.search_path {
            for extension in &self.extensions {
                let filename = basedir.join(Path::new(name).with_extension(extension));
                if (self.check_exists)(&filename) {
                    return Some(filename);
                }
            }
        }
        None
    }
}
