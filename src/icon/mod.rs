//! Based on Icon Theme Specification 0.11
//! https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-0.11.html

use crate::{expand_path, xdg_data_search_paths};
use std::path::PathBuf;

mod find;


// Icon Theme default paths
const XDG_ICON_DIR: &str = "icons";
const ICON_THEME_FIRST_DIR: &str = "~/.icons";
const ICON_THEME_LAST_DIR: &str = "/usr/share/pixmaps";

/// Return a vector of paths to be searched for icons
pub fn icon_search_paths() -> Vec<PathBuf> {
    let mut path: Vec<_> = xdg_data_search_paths()
        .iter()
        .map(|i| i.join(XDG_ICON_DIR))
        .collect();
    path.insert(0, expand_path(PathBuf::from(ICON_THEME_FIRST_DIR)));
    path.push(expand_path(PathBuf::from(ICON_THEME_LAST_DIR)));
    path
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    pub(crate) fn test_icon_search_paths() {
        assert_eq!(
            icon_search_paths(),
            vec![
                PathBuf::from("/root/.icons"),
                PathBuf::from("/root/.local/share/icons"),
                PathBuf::from("/usr/local/share/icons"),
                PathBuf::from("/usr/share/icons"),
                PathBuf::from("/usr/share/pixmaps"),
            ]
        );
    }
}
