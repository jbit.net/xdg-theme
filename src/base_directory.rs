//! Path handling code
//!
//! Based on XDG Base Directory Specification Version 0.7
//! https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.7.html
//!
//! All environment variable and path handling should use `OsStr` instead of `str` to support non-UTF8 systems
#![allow(dead_code)]

use crate::{env_home_path, env_search_path, expand_path};
use std::path::PathBuf;

pub(crate) const XDG_DATA_HOME: &str = "XDG_DATA_HOME";
pub(crate) const XDG_CONFIG_HOME: &str = "XDG_CONFIG_HOME";
pub(crate) const XDG_CACHE_HOME: &str = "XDG_CACHE_HOME";
pub(crate) const XDG_DATA_DIRS: &str = "XDG_DATA_DIRS";
pub(crate) const XDG_CONFIG_DIRS: &str = "XDG_CONFIG_DIRS";
pub(crate) const XDG_RUNTIME_DIR: &str = "XDG_RUNTIME_DIR";

const XDG_DATA_HOME_DEFAULT: &str = "~/.local/share";
const XDG_CONFIG_HOME_DEFAULT: &str = "~/.config";
const XDG_CACHE_HOME_DEFAULT: &str = "~/.cache";
const XDG_DATA_DIRS_DEFAULT: &str = "/usr/local/share:/usr/share";
const XDG_CONFIG_DIRS_DEFAULT: &str = "/etc/xdg";

/// Return a path value of XDG_DATA_HOME
pub fn xdg_data_home() -> PathBuf {
    env_home_path(XDG_DATA_HOME, XDG_DATA_HOME_DEFAULT)
}

/// Return a path value of XDG_CONFIG_HOME
pub fn xdg_config_home() -> PathBuf {
    env_home_path(XDG_CONFIG_HOME, XDG_CONFIG_HOME_DEFAULT)
}

/// Return a path value of XDG_CACHE_HOME
pub fn xdg_cache_home() -> PathBuf {
    env_home_path(XDG_CACHE_HOME, XDG_CACHE_HOME_DEFAULT)
}

/// Return a possible path value of XDG_RUNTIME_DIR
pub fn xdg_runtime_dir() -> Option<PathBuf> {
    std::env::var_os(XDG_RUNTIME_DIR)
        .filter(|var| !var.is_empty())
        .map(PathBuf::from)
        .map(expand_path)
}

/// Return a vector of paths to be searched for data
/// This is XDG_DATA_HOME + XDG_DATA_DIRS
pub fn xdg_data_search_paths() -> Vec<PathBuf> {
    let mut path = env_search_path(XDG_DATA_DIRS, XDG_DATA_DIRS_DEFAULT);
    path.insert(0, xdg_data_home());
    path
}

/// Return a vector of paths to be searched for data
/// This is XDG_CONFIG_HOME + XDG_CONFIG_DIRS
pub fn xdg_config_search_paths() -> Vec<PathBuf> {
    let mut path = env_search_path(XDG_CONFIG_DIRS, XDG_CONFIG_DIRS_DEFAULT);
    path.insert(0, xdg_config_home());
    path
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use std::env::{remove_var, set_var};

    pub(crate) fn test_cache_path() {
        // Test default states
        remove_var(XDG_CACHE_HOME);
        assert_eq!(xdg_cache_home(), PathBuf::from("/root/.cache"));

        set_var(XDG_CACHE_HOME, "");
        assert_eq!(xdg_cache_home(), PathBuf::from("/root/.cache"));

        // Override home
        set_var(XDG_CACHE_HOME, "~");
        assert_eq!(xdg_cache_home(), PathBuf::from("/root"));

        set_var(XDG_CACHE_HOME, "~test");
        assert_eq!(xdg_cache_home(), PathBuf::from("~test"));

        set_var(XDG_CACHE_HOME, "~/XDG Cache");
        assert_eq!(xdg_cache_home(), PathBuf::from("/root/XDG Cache"));
    }

    pub(crate) fn test_data_paths() {
        // Test default states
        remove_var(XDG_DATA_HOME);
        remove_var(XDG_DATA_DIRS);
        assert_eq!(xdg_data_home(), PathBuf::from("/root/.local/share"));
        assert_eq!(
            xdg_data_search_paths(),
            vec![
                PathBuf::from("/root/.local/share"),
                PathBuf::from("/usr/local/share"),
                PathBuf::from("/usr/share"),
            ]
        );

        set_var(XDG_DATA_HOME, "");
        set_var(XDG_DATA_DIRS, "");
        assert_eq!(xdg_data_home(), PathBuf::from("/root/.local/share"));
        assert_eq!(
            xdg_data_search_paths(),
            vec![
                PathBuf::from("/root/.local/share"),
                PathBuf::from("/usr/local/share"),
                PathBuf::from("/usr/share"),
            ]
        );

        // Override home
        set_var(XDG_DATA_HOME, "~");
        assert_eq!(xdg_data_home(), PathBuf::from("/root"));

        set_var(XDG_DATA_HOME, "~test");
        assert_eq!(xdg_data_home(), PathBuf::from("~test"));

        set_var(XDG_DATA_HOME, "~/XDG Data");
        assert_eq!(xdg_data_home(), PathBuf::from("/root/XDG Data"));

        // Override searchpath
        set_var(XDG_DATA_DIRS, ":/opt/share::/usr/share:");
        assert_eq!(
            xdg_data_search_paths(),
            vec![
                PathBuf::from("/root/XDG Data"),
                PathBuf::from("/opt/share"),
                PathBuf::from("/usr/share"),
            ]
        );
    }

    pub(crate) fn test_config_paths() {
        // Test default states
        remove_var(XDG_CONFIG_HOME);
        remove_var(XDG_CONFIG_DIRS);
        assert_eq!(xdg_config_home(), PathBuf::from("/root/.config"));
        assert_eq!(
            xdg_config_search_paths(),
            vec![PathBuf::from("/root/.config"), PathBuf::from("/etc/xdg"),]
        );

        set_var(XDG_CONFIG_HOME, "");
        set_var(XDG_CONFIG_DIRS, "");
        assert_eq!(xdg_config_home(), PathBuf::from("/root/.config"));
        assert_eq!(
            xdg_config_search_paths(),
            vec![PathBuf::from("/root/.config"), PathBuf::from("/etc/xdg"),]
        );

        // Override home
        set_var(XDG_CONFIG_HOME, "~");
        assert_eq!(xdg_config_home(), PathBuf::from("/root"));

        set_var(XDG_CONFIG_HOME, "~test");
        assert_eq!(xdg_config_home(), PathBuf::from("~test"));

        set_var(XDG_CONFIG_HOME, "~/XDG Config");
        assert_eq!(xdg_config_home(), PathBuf::from("/root/XDG Config"));

        // Override searchpath
        set_var(XDG_CONFIG_DIRS, ":/opt/etc/xdg::/etc/xdg:");
        assert_eq!(
            xdg_config_search_paths(),
            vec![
                PathBuf::from("/root/XDG Config"),
                PathBuf::from("/opt/etc/xdg"),
                PathBuf::from("/etc/xdg"),
            ]
        );
    }

    pub(crate) fn test_runtime_path() {
        // Test default states
        remove_var(XDG_RUNTIME_DIR);
        assert_eq!(xdg_runtime_dir(), None);

        set_var(XDG_RUNTIME_DIR, "");
        assert_eq!(xdg_runtime_dir(), None);

        // Test home relative path
        set_var(XDG_RUNTIME_DIR, "~/XDG Runtime");
        assert_eq!(xdg_runtime_dir(), Some(PathBuf::from("/root/XDG Runtime")));

        // Test absolute path
        set_var(XDG_RUNTIME_DIR, "/var/run/0/xdg");
        assert_eq!(xdg_runtime_dir(), Some(PathBuf::from("/var/run/0/xdg")));
    }
}
