//! Xcursor File Loading
//!
//! Based on format described in Xcursor(3), assuming little-endian.
//! https://www.x.org/releases/X11R7.7/doc/man/man3/Xcursor.3.xhtml

use crate::invalid_data_error;
use std::fs::File;
use std::io::{BufReader, Read, Result as IOResult, Seek, SeekFrom};
use std::path::Path;

const XCUR_MAJOR_VERSION: u32 = 1;

const HEADER_MAGIC: u32 = 0x72_75_63_58; // b"Xcur"
const HEADER_SIZE: u32 = 4 * 4;
const TOC_ENTRY_SIZE: u32 = 4 * 3;

const ENTRY_TYPE_COMMENT: u32 = 0xfffe_0001;
const ENTRY_TYPE_IMAGE: u32 = 0xfffd_0002;

const COMMENT_HEADER_SIZE: u32 = 4 * 5;
const COMMENT_COPYRIGHT: u32 = 1;
const COMMENT_LICENSE: u32 = 2;
const COMMENT_OTHER: u32 = 3;
const COMMENT_MAX_SIZE: u32 = 64 * 1024;

const IMAGE_HEADER_SIZE: u32 = 4 * 9;
const IMAGE_MAX_SIZE: u32 = 1024;

#[derive(Debug)]
pub struct XcursorImage {
    pub size: u32,
    pub width: u32,
    pub height: u32,
    pub xhot: u32,
    pub yhot: u32,
    pub delay: u32,
    pub pixels: Vec<u8>,
}

#[derive(Debug)]
pub struct XcursorFile {
    version: u32,
    copyright: Option<String>,
    license: Option<String>,
    comment: Option<String>,
    images: Vec<XcursorImage>,
}

fn read_u32(stream: &mut Read) -> IOResult<u32> {
    let mut buf = [0, 0, 0, 0];
    stream.read_exact(&mut buf)?;
    Ok(u32::from_le_bytes(buf))
}

impl XcursorFile {
    /// Load an Xcur formatted file from a file
    pub fn load_file(path: &Path) -> IOResult<XcursorFile> {
        let file = File::open(path)?;
        let mut reader = BufReader::new(file);
        XcursorFile::load(&mut reader)
    }

    /// Load an Xcur formatted file from a stream
    pub fn load<T: Read + Seek>(stream: &mut T) -> IOResult<XcursorFile> {
        // Get current position in stream
        let start_position = stream.seek(SeekFrom::Current(0))?;

        // Read in header
        let magic = read_u32(stream)?;
        let header_size = read_u32(stream)?;
        let version = read_u32(stream)?;
        let toc_length = read_u32(stream)?;

        if magic != HEADER_MAGIC {
            return invalid_data_error!("Invalid Xcur magic (0x{:08x})", magic);
        }

        if header_size < HEADER_SIZE {
            return invalid_data_error!("Invalid Xcur header size ({})", header_size);
        }

        let mut cursor_file = XcursorFile {
            version,
            copyright: None,
            license: None,
            comment: None,
            images: vec![],
        };

        if cursor_file.version().0 != XCUR_MAJOR_VERSION {
            // Major version changes are "incompatible"
            return invalid_data_error!(
                "Unsupported Xcur version ({}.{})",
                cursor_file.version().0,
                cursor_file.version().1
            );
        }

        // Load in the TOC and entries at the same time
        let toc_begin = header_size;
        let toc_end = toc_begin + (TOC_ENTRY_SIZE * toc_length);

        for toc_id in 0..toc_length {
            // Seek to the toc entry
            let toc_offset = toc_begin + (TOC_ENTRY_SIZE * toc_id);
            stream.seek(SeekFrom::Start(start_position + u64::from(toc_offset)))?;

            // Read in toc entry
            let entry_type = read_u32(stream)?;
            let entry_subtype = read_u32(stream)?;
            let entry_offset = read_u32(stream)?;

            if entry_offset < toc_end {
                return invalid_data_error!(
                    "Invalid Xcur TOC #{} ({} < {})",
                    toc_id,
                    entry_offset,
                    toc_end
                );
            }

            // Read in the entry data
            stream.seek(SeekFrom::Start(start_position + u64::from(entry_offset)))?;
            match (entry_type, entry_subtype) {
                (ENTRY_TYPE_COMMENT, COMMENT_COPYRIGHT) => {
                    cursor_file.copyright = Some(Self::load_comment(stream, entry_subtype)?);
                }
                (ENTRY_TYPE_COMMENT, COMMENT_LICENSE) => {
                    cursor_file.license = Some(Self::load_comment(stream, entry_subtype)?);
                }
                (ENTRY_TYPE_COMMENT, COMMENT_OTHER) => {
                    cursor_file.comment = Some(Self::load_comment(stream, entry_subtype)?);
                }
                (ENTRY_TYPE_IMAGE, _) => {
                    let image = Self::load_image(stream, entry_subtype)?;
                    cursor_file.images.push(image);
                }
                _ => {} // Ignore unknown TOC types
            }
        }

        Ok(cursor_file)
    }

    /// Load in a comment chunk from a stream
    fn load_comment(stream: &mut Read, expected_subtype: u32) -> IOResult<String> {
        // Read in comment header
        let header_size = read_u32(stream)?;
        let entry_type = read_u32(stream)?;
        let subtype = read_u32(stream)?;
        let version = read_u32(stream)?;
        let length = read_u32(stream)?;

        if header_size != COMMENT_HEADER_SIZE {
            return invalid_data_error!("Invalid Xcur comment header size ({})", header_size);
        }
        if entry_type != ENTRY_TYPE_COMMENT {
            return invalid_data_error!("Invalid Xcur comment type (0x{:08x})", entry_type);
        }
        if subtype != expected_subtype {
            return invalid_data_error!(
                "Invalid Xcur comment subtype ({}, expecting {})",
                subtype,
                expected_subtype
            );
        }
        if version != 1 {
            return invalid_data_error!("Unsupported Xcur comment version ({})", version);
        }
        if length > COMMENT_MAX_SIZE {
            return invalid_data_error!("Invalid Xcur comment length ({})", length);
        }

        // Read in the string
        let mut data = vec![0u8; length as usize];
        stream.read_exact(&mut data)?;

        // Remove nul termination if it exists
        while data.ends_with(&[0u8]) {
            data.pop();
        }

        Ok(String::from_utf8_lossy(&data).to_string())
    }

    /// Load in an image chunk from a stream
    fn load_image(stream: &mut Read, expected_size: u32) -> IOResult<XcursorImage> {
        // Read in image header
        let header_size = read_u32(stream)?;
        let entry_type = read_u32(stream)?;
        let size = read_u32(stream)?;
        let version = read_u32(stream)?;
        let width = read_u32(stream)?;
        let height = read_u32(stream)?;
        let xhot = read_u32(stream)?;
        let yhot = read_u32(stream)?;
        let delay = read_u32(stream)?;

        if header_size != IMAGE_HEADER_SIZE {
            return invalid_data_error!("Invalid Xcur image header size ({})", header_size);
        }
        if entry_type != ENTRY_TYPE_IMAGE {
            return invalid_data_error!("Invalid Xcur image type (0x{:08x})", entry_type);
        }
        if size != expected_size {
            return invalid_data_error!("Invalid Xcur image size ({})", size);
        }
        if version != 1 {
            return invalid_data_error!("Unsupported Xcur image version ({})", version);
        }
        if width == 0 || width > IMAGE_MAX_SIZE || height == 0 || height > IMAGE_MAX_SIZE {
            return invalid_data_error!("Invalid Xcur image size ({}x{})", width, height);
        }
        if xhot >= width || yhot >= height {
            return invalid_data_error!("Invalid Xcur hotspot ({},{})", xhot, yhot);
        }

        // Load pixels
        let mut pixels = vec![0u8; (width * height * 4) as usize];

        stream.read_exact(&mut pixels)?;

        Ok(XcursorImage {
            size,
            width,
            height,
            xhot,
            yhot,
            delay,
            pixels,
        })
    }

    /// Get copyright comment from loaded file
    pub fn copyright(&self) -> Option<&str> {
        self.copyright.as_ref().map(String::as_str)
    }

    /// Get license comment from loaded file
    pub fn license(&self) -> Option<&str> {
        self.license.as_ref().map(String::as_str)
    }

    /// Get "other" comment from loaded file
    pub fn comment(&self) -> Option<&str> {
        self.comment.as_ref().map(String::as_str)
    }

    /// Get version of the loaded file as a (MAJOR, MINOR) tuple
    pub fn version(&self) -> (u32, u32) {
        ((self.version >> 16) & 0xffff, self.version & 0xffff)
    }

    /// Get vector of images read from this file
    pub fn images(&self) -> &Vec<XcursorImage> {
        &self.images
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::{Cursor, ErrorKind};

    #[test]
    fn minimal_file() {
        #[rustfmt::skip]
        let data = [
            /* magic   */ b'X', b'c', b'u', b'r',
            /* header  */ 16, 0, 0, 0,
            /* version */ 0, 0, 1, 0,
            /* ntoc    */ 4, 0, 0, 0,
            /* toc0    */ 0x01, 0x00, 0xfe, 0xff, 1, 0, 0, 0, 16 + (12 * 4), 0, 0, 0,
            /* toc1    */ 0x01, 0x00, 0xfe, 0xff, 2, 0, 0, 0, 16 + (12 * 4) + 30, 0, 0, 0,
            /* toc2    */ 0x01, 0x00, 0xfe, 0xff, 3, 0, 0, 0, 16 + (12 * 4) + 30 + 27, 0, 0, 0,
            /* toc3    */ 0x02, 0x00, 0xfd, 0xff, 4, 0, 0, 0, 16 + (12 * 4) + 30 + 27 + 25, 0, 0, 0,

            /* copyright chunk header */
            20, 0, 0, 0, 0x01, 0x00, 0xfe, 0xff, 1, 0, 0, 0, 1, 0, 0, 0,
            /* copyright comment header */
            10, 0, 0, 0,
            /* copyright string */
            b'C', b'o', b'p', b'y', b'r', b'i', b'g', b'h', b't', b'\0',

            /* license chunk header */
            20, 0, 0, 0, 0x01, 0x00, 0xfe, 0xff, 2, 0, 0, 0, 1, 0, 0, 0,
            /* license comment header */
            7, 0, 0, 0,
            /* license string */
            b'L', b'i', b'c', b'e', b'n', b's', b'e',

            /* other comment chunk header */
            20, 0, 0, 0, 0x01, 0x00, 0xfe, 0xff, 3, 0, 0, 0, 1, 0, 0, 0,
            /* other comment header */
            5, 0, 0, 0,
            /* other comment string */
            b'O', b't', b'h', b'e', b'r',

            /* image chunk header */
            36, 0, 0, 0, 0x02, 0x00, 0xfd, 0xff, 4, 0, 0, 0, 1, 0, 0, 0,
            /* image header */
            1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 16, 0, 0, 0,
            /* image data */
            0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80,
        ].as_ref();
        let mut stream = Cursor::new(data);
        let xcursor = XcursorFile::load(&mut stream);
        assert!(xcursor.is_ok());
        let xcursor = xcursor.unwrap();
        assert_eq!(xcursor.version(), (1, 0));
        assert_eq!(xcursor.copyright(), Some("Copyright"));
        assert_eq!(xcursor.license(), Some("License"));
        assert_eq!(xcursor.comment(), Some("Other"));

        let images = xcursor.images();
        assert_eq!(images.len(), 1);
        assert_eq!(images[0].size, 4);
        assert_eq!(images[0].width, 1);
        assert_eq!(images[0].height, 2);
        assert_eq!(images[0].xhot, 0);
        assert_eq!(images[0].yhot, 1);
        assert_eq!(images[0].delay, 16);
        assert_eq!(images[0].pixels.len(), 8);
        assert_eq!(
            images[0].pixels,
            [0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80]
        );
    }

    #[test]
    fn empty_toc() {
        #[rustfmt::skip]
        let data = [
            /* magic   */ b'X', b'c', b'u', b'r',
            /* header  */ 16, 0, 0, 0,
            /* version */ 50, 0, 1, 0,
            /* ntoc    */ 0, 0, 0, 0,
        ].as_ref();
        let mut stream = Cursor::new(data);
        let xcursor = XcursorFile::load(&mut stream);
        assert!(xcursor.is_ok());
        let xcursor = xcursor.unwrap();
        assert_eq!(xcursor.version(), (1, 50));
        assert_eq!(xcursor.images().len(), 0);
    }

    #[test]
    fn error_unsupported_version() {
        #[rustfmt::skip]
        let data = [
            /* magic   */ b'X', b'c', b'u', b'r',
            /* header  */ 16, 0, 0, 0,
            /* version */ 0, 0, 2, 0,
            /* ntoc    */ 0, 0, 0, 0,
        ].as_ref();
        let mut stream = Cursor::new(data);
        let xcursor = XcursorFile::load(&mut stream);
        assert!(xcursor.is_err());
        assert_eq!(xcursor.unwrap_err().kind(), ErrorKind::InvalidData);
    }

    #[test]
    fn error_no_data() {
        let mut stream = Cursor::new([].as_ref());
        let xcursor = XcursorFile::load(&mut stream);
        assert!(xcursor.is_err());
        assert_eq!(xcursor.unwrap_err().kind(), ErrorKind::UnexpectedEof);
    }

    #[test]
    fn error_zero_data() {
        let mut stream = Cursor::new([0u8; 256].as_ref());
        let xcursor = XcursorFile::load(&mut stream);
        assert!(xcursor.is_err());
        assert_eq!(xcursor.unwrap_err().kind(), ErrorKind::InvalidData);
    }
}
