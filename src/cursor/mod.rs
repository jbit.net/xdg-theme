//! Based on information described in Xcursor(3).
//! https://www.x.org/releases/X11R7.7/doc/man/man3/Xcursor.3.xhtml

mod xcursor_file;
pub use xcursor_file::*;

use crate::env_search_path;
use crate::icon::icon_search_paths;
use std::path::PathBuf;

// Xcursor environment variable
const XCURSOR_PATH: &str = "XCURSOR_PATH";

/// Return a vector of paths to be searched for cursor
pub fn cursor_search_paths() -> Vec<PathBuf> {
    let paths = env_search_path(XCURSOR_PATH, "");
    if paths.is_empty() {
        // The Xcursor spec says to only look at the following by default:
        //    ~/.icons, /usr/share/icons, /usr/share/pixmaps
        // But instead, let's default to the XDG icon search path
        icon_search_paths()
    } else {
        paths
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;
    use std::env::{remove_var, set_var};

    pub(crate) fn test_cursor_search_paths() {
        remove_var(XCURSOR_PATH);
        assert_eq!(
            cursor_search_paths(),
            vec![
                PathBuf::from("/root/.icons"),
                PathBuf::from("/root/.local/share/icons"),
                PathBuf::from("/usr/local/share/icons"),
                PathBuf::from("/usr/share/icons"),
                PathBuf::from("/usr/share/pixmaps"),
            ]
        );

        set_var(
            XCURSOR_PATH,
            ":::~/Xcursor Themes:~/.icons:/usr/share/icons::/usr/share/pixmaps:",
        );
        assert_eq!(
            cursor_search_paths(),
            vec![
                PathBuf::from("/root/Xcursor Themes"),
                PathBuf::from("/root/.icons"),
                PathBuf::from("/usr/share/icons"),
                PathBuf::from("/usr/share/pixmaps"),
            ]
        );
    }
}
