mod base_directory;
pub use base_directory::*;

mod ini;
pub use ini::*;

pub mod cursor;
pub mod icon;

use std::path::{Path, PathBuf};

pub(crate) fn invalid_data_error<T>(message: String) -> std::io::Result<T> {
    Err(std::io::Error::new(
        std::io::ErrorKind::InvalidData,
        message,
    ))
}

#[macro_export]
macro_rules! invalid_data_error {
    ($($arg:tt)*) => ({ $crate::invalid_data_error(format!($($arg)*)) })
}

pub(crate) fn expand_path(path: PathBuf) -> PathBuf {
    // Paths beginning with ~/ (or are only "~") get replaced with home directory
    if let Ok(relative_path) = path.strip_prefix("~") {
        if let Some(home) = std::env::var_os("HOME").filter(|var| !var.is_empty()) {
            let path = Path::new(&home).join(relative_path);
            return path;
        }
    }

    // Otherwise, return the existing path unchanged
    path
}

pub(crate) fn env_search_path(variable_name: &str, default: &str) -> Vec<PathBuf> {
    let variable_value = std::env::var_os(variable_name)
        .filter(|var| !var.is_empty())
        .unwrap_or_else(|| default.into());
    std::env::split_paths(&variable_value)
        .filter(|var| !var.as_os_str().is_empty())
        .map(expand_path)
        .collect()
}

pub(crate) fn env_home_path(variable_name: &str, default: &str) -> PathBuf {
    let variable_value = std::env::var_os(variable_name)
        .filter(|var| !var.is_empty())
        .unwrap_or_else(|| default.into());
    expand_path(PathBuf::from(variable_value))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::base_directory::tests::*;
    use crate::cursor::tests::*;
    use crate::icon::tests::*;
    use std::env::{remove_var, set_var};

    // These need to be in one big test because it modifies global state (environment variables) :(
    #[test]
    fn test() {
        reset_variables();
        test_expand_path();

        reset_variables();
        test_cache_path();

        reset_variables();
        test_data_paths();

        reset_variables();
        test_config_paths();

        reset_variables();
        test_runtime_path();

        reset_variables();
        test_icon_search_paths();

        reset_variables();
        test_cursor_search_paths();
    }

    fn reset_variables() {
        set_var("HOME", "/root");
        remove_var(XDG_DATA_HOME);
        remove_var(XDG_CONFIG_HOME);
        remove_var(XDG_CACHE_HOME);
        remove_var(XDG_DATA_DIRS);
        remove_var(XDG_CONFIG_DIRS);
    }

    fn test_expand_path() {
        assert_eq!(expand_path(PathBuf::from("")), PathBuf::from(""));
        assert_eq!(expand_path(PathBuf::from("~")), PathBuf::from("/root"));
        assert_eq!(expand_path(PathBuf::from("~/")), PathBuf::from("/root/"));
        assert_eq!(
            expand_path(PathBuf::from("~/.config")),
            PathBuf::from("/root/.config")
        );
        assert_eq!(
            expand_path(PathBuf::from("/root/~/")),
            PathBuf::from("/root/~/")
        );
        assert_eq!(
            expand_path(PathBuf::from("~user/")),
            PathBuf::from("~user/")
        );
    }
}
